<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix'=>'/main'],function(){
    Route::post('/login','MainController@core_main_login');
    //Route::get('/logout','MainController@core_main_logout');
    Route::get('/get/mainmenu','MainController@core_main_get_mainmenu');
    Route::get('/get/submenu/{id_menu}','MainController@core_main_get_submenu');
});

Route::group(['prefix'=>'/produto','middleware'=>'basic'],function() {
    Route::post('/create','ProdutoController@core_product_create_product');
    Route::post('/update','ProdutoController@core_product_update_product');
    Route::get('/get/list','ProdutoController@core_product_list_product');
    Route::get('/get/{id_produto}','ProdutoController@core_product_get_product');
    Route::get('/delete/{id_produto}','ProdutoController@core_product_delete_product');
});

Route::group(['prefix'=>'/list','middleware'=>'basic'],function(){
    Route::get('/products','ProdutoController@core_product_list_product');
});

Route::group(['prefix'=>'/get','middleware'=>'basic'],function(){
    Route::get('/products/{id_produto}','ProdutoController@core_product_get_product');
});

