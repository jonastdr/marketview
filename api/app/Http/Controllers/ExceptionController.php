<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use PhpSpec\Exception\Exception;

class ExceptionController extends Exception
{
    private $erros = '';

    public function __construct($e)
    {
        $this->erros = "Error: \n\r";
        if(is_array($e)) {
            foreach ($e as $erro) {
                $this->erros .= $erro . "\n\r";
            }
        }else{
            $this->erros = 'Error fail, exception receive only parameter format array';
        }
    }

    public function exception()
    {
        return $this->erros;
    }


}
