<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{

	// serviços main apenas para pessoas autorizadas
	public function core_main_get_mainmenu(){}
	public function core_main_get_submenu(){}
	public function core_main_login(){}
	public function core_main_logout(){}
	public function core_main_create_mainmenu(){}
	public function core_main_update_mainmenu(){}
	public function core_main_delete_mainmenu(){}
	public function core_main_get_user_ystem(){}
	public function core_main_create_user_ystem(){}
	public function core_main_update_user_ystem(){}
	public function core_main_delete_user_ystem(){}
	public function core_main_get_admin(){}
	public function core_main_create_admin(){}
	public function core_main_update_admin(){}
	public function core_main_get_user(){}

	public function core_user_get_user(){}
	public function core_user_create_user(){}
	public function core_user_update_user(){}
	public function core_user_delete_user(){}

	//lisat para usuario
	public function core_list_get_product(){}
	public function core_list_get_store(){}
	public function core_list_get_history(){}

	public function core_message_get_message(){}
	public function core_message_create_message(){}
	public function core_message_update_message(){}
	public function core_message_delete_message(){}

	//para consulta do usuario (nao usuario_sistema)
	public function core_store_get_store(){}
	public function core_store_create_store(){}
	public function core_store_update_store(){}
	public function core_store_delete_store(){}
	public function core_store_search_store(){} // diferente da get ele procura utilizando as tag

	public function core_product_get_product(){}
	public function core_product_create_product(){}
	public function core_product_update_product(){}
	public function core_product_delete_product(){}
	public function core_product_list_product(){}
	public function core_product_search_product(){} // diferente da get ele procura utilizando as tag

	// tag pode ser tanto de produto quando de lojas de ramos diferentes
	public function core_tag_get_tag(){}
	public function core_tag_create_tag(){}
	public function core_tag_update_tag(){}
	public function core_tag_delete_tag(){}

}
