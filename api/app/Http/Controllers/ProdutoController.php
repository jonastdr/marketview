<?php

namespace App\Http\Controllers;

use App\Models\Produtos;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProdutoController extends Controller
{
	/**
   * Method receive GET
   * @param Integer $id_produto id produto for get details
   * @return Object mixed Return produto by receive id
   */
    public function core_product_get_product($id_produto)
    {
        $produto = new Produtos();
        $result = $produto->where('id_produto','=',$id_produto)->get()->toJson();
        return $result;
    }

    /**
     * Method receive POST
     * @return String string Retrun result operation
     */
    public function core_product_create_product()
    {
        $arg = \Request::all();

        $erro = [];
        $create = [];
        if (isset($arg['descricao']) && isset($arg['ativo']) && isset($arg['valor']) && isset($arg['id_loja'])) {
            if (strlen($arg['descricao']) < 3 && strlen($arg['descricao']) > 100) {
                $erro[] = 'Descricao invalid, this parameter needle min 3 or max 100 caracters';
            } else {
                $create['descricao'] = $arg['descricao'];
            }
            if (strlen($arg['ativo']) != 1 || is_numeric($arg['ativo']) == false) {
                $erro[] = 'Ativo invalid, this parameter is integer and only 1 caracter';
            } else {
                $create['ativo'] = $arg['ativo'];
            }
            if (strlen($arg['id_loja']) != 1 || is_numeric($arg['id_loja']) == false) {
                $erro[] = 'Id_loja invalid, this parameter is integer and only 1 caracter';
            } else {
                $create['ativo'] = $arg['ativo'];
            }
            if (is_numeric($arg['valor']) == false && $arg['valor'] > 0) {
                $erro[] = 'Valor invalid, this parameter is numeric or float';
            } else {
                $create['valor'] = $arg['valor'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'descricao','ativo','valor','id_loja': array";
        }
        if (count($erro) == 0) {

            $produto = new Produtos();

            if ($produto->create($create)) {
                return 'Produto create success';
            } else {
                return 'Internal error create produto';
            }
        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }

    /**
     * Method receive POST
     * @return String string return result operation
     */
    public function core_product_update_product()
    {
        $arg = \Request::all();

        $erro = [];
        $create = [];
        if (isset($arg['id_produto'])  && isset($arg['descricao']) && isset($arg['ativo']) && isset($arg['valor']) && isset($arg['id_loja'])) {
            if (strlen($arg['id_produto']) != 1 || is_numeric($arg['id_loja']) == false) {
                $erro[] = 'Id_produto invalid, this parameter is integer and only 1 caracter';
            }
            if (strlen($arg['descricao']) < 3 && strlen($arg['descricao']) > 100) {
                $erro[] = 'Descricao invalid, this parameter needle min 3 or max 100 caracters';
            } else {
                $create['descricao'] = $arg['descricao'];
            }
            if (strlen($arg['ativo']) != 1 || is_numeric($arg['ativo']) == false) {
                $erro[] = 'Ativo invalid, this parameter is integer and only 1 caracter';
            } else {
                $create['ativo'] = $arg['ativo'];
            }
            if (strlen($arg['id_loja']) != 1 || is_numeric($arg['id_loja']) == false) {
                $erro[] = 'Id_loja invalid, this parameter is integer and only 1 caracter';
            } else {
                $create['ativo'] = $arg['ativo'];
            }
            if (is_numeric($arg['valor']) == false && $arg['valor'] > 0) {
                $erro[] = 'Valor invalid, this parameter is numeric or float';
            } else {
                $create['valor'] = $arg['valor'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'id_produto','descricao','ativo','valor','id_loja': array";
        }
        if (count($erro) == 0) {

            $produto = new Produtos();

            if ($produto->where('id_produto','=',$arg['id_produto'])->update($create)) {
                return 'Produto update success';
            } else {
                return 'Internal error update produto';
            }
        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }

    /**
     * Method receive GET
     * @param Interger $id_produto id produto for delete produto
     * @return String string return result operation
     */
    public function core_product_delete_product($id_produto)
    {
        $produto = new Produtos();
        $produto->where('id_produto','=',$id_produto)->delete();
        return 'Produto delete success';
    }

    /**
     * Method receive GET
     * @return Object mixed return list with all product
     */
    public function core_product_list_product()
    {
        $produtos = new Produtos();
        $result = $produtos->where('ativo','=',true)->get()->toJson();
        return $result;
    }

    /**
     *
     */
    public function core_product_search_product()
    {
        // building
    }
}
