<?php

namespace App\Http\Controllers;

use App\Models\MainMenu;
use App\Models\UsuarioSistema;
use Auth;
use App\Http\Requests;

class MainController extends Controller
{
	/**
   * Return all main menu from store config
   * @return mixed
   */
    public function core_main_get_mainmenu()
    {
        $mainMenu = new MainMenu();
        //nao possui campo ativo
        $result = $mainMenu->where('ativo','=',true)->get()->toJson();
        return $result;
    }

    /**
     * Return all sub menus form menus sending for parameters
     * @param $id_menu
     * @return mixed
     */
    public function core_main_get_submenu($id_menu)
    {
        $subMenu = new SubMenu();
        $result = $subMenu->where('id_menu','=',$id_menu)->get()->toJson();
        return $result;
    }

    public function core_main_login()
    {
        $arg = \Request::all();

        if(isset($arg['remember']) == false){
            $arg['remember'] = false;
        }

        $erro = [];
        if(strlen($arg['login']) < 5){
            $erro[] = 'Erro:Login invalid, parameter login neede min. 6 caracter';
        }
        if(strlen($arg['login']) > 10){
            $erro[] = 'Erro:Login invalid, parameter login neede max. 10 caracter';
        }
        if(strlen($arg['senha']) < 5){
            $erro[] = 'Erro:Password invalid, parameter password neede min. 5 caracter';
        }
        if(strlen($arg['senha']) > 15){
            $erro[] = 'Erro:Password invalid, parameter password neede max. 15 caracter';
        }
        if(is_bool($arg['remember']) == FALSE){
            $erro[] = 'Erro:Remenber invalid format, parameter Remember acepet only format bool';
        }

        if(count($erro) == 0){
            $params = [
              'login'       =>  $arg['login'],
              'senha'    =>  $arg['senha']
            ];
            if(Auth::attempt($params,$arg['remember'])){
                return Auth::user();
            }

        }else{
            $e = new ExceptionController($erro);
            return $e->exception();
        }

    }

    public function core_main_logout()
    {
        //finaliza e salva oq foi feito ate o momento nesta sessao
        return 'logout ....';
    }

    public function core_main_create_mainmenu()
    {
        $arg = \Request::all();

        $erro = [];
        $create = [];
        if (isset($arg['menu'])) {
            if (strlen($arg['menu']) < 3) {
                $erro[] = 'Name menu invalid, this parameter needle min 3 caracter';
            } else {
                $create['menu'] = $arg['menu'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'menu',...";
        }
        if (count($erro) > 0) {

            $menu = new MainMenu();

            if ($menu->create($create)) {
                return 'Menu create success';
            } else {
                return 'Internal error create menu';
            }
        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }

    }

    public function core_main_update_mainmenu()
    {
        $arg = \Request::all();
        $erro = [];

        if (strlen($arg['menu']) < 3) {
            $erro[] = 'Name menu invalid, this parameter needle min 3 caracters';
        } elseif(strlen($arg['menu']) > 10) {
            $erro[] = 'name menu invalid, this parameter needle max 10 caracters';
        }else{
            $create['menu'] = $arg['menu'];
        }

        if (count($erro) == 0) {

            $menu = new MainMenu();

            if ($menu->where('id_menu','=',$arg['id_menu'])->update($create)) {
                return 'Menu edite success';
            } else {
                return 'Internal error edite menu';
            }
        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }

    }

    public function core_main_delete_mainmenu()
    {
        $arg = \Request::all();

        $menu = new MainMenu();

        if ($menu->where('id_menu','=',$arg['id_menu'])->delete()) {
            return 'Menu delete success';
        } else {
            return 'Internal error delete menu';
        }

    }

    public function core_main_get_user_system()
    {
        $usuario_sistema = new UsuarioSistema();
        $result = $usuario_sistema->where('ativo','=',true)->get()->toJson();
        return $result;
    }

    public function core_main_create_user_system()
    {
        $arg = \Request::all();

        $erro = [];
        $create = [];
        if (isset($arg['nome']) && isset($arg['login']) && isset($arg['senha']) && isset($arg['permicao'])) {
            if (strlen($arg['nome']) < 3 && strlen($arg['nome']) > 10) {
                $erro[] = 'Name invalid, this parameter needle min 3 or max 10 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
            if (strlen($arg['nome']) < 3) {
                $erro[] = 'Name invalid, this parameter needle min 3 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'nome','login','senha','permicao': array";
        }
        if (count($erro) == 0) {

            $userSystem = new UsuarioSistema();

            if ($userSystem->create($create)) {
                return 'User system create success';
            } else {
                return 'Internal error create user system';
            }
        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }

    public function core_main_update_user_system()
    {
        $arg = \Request::all();
        $erro = [];
        if (isset($arg['nome']) && isset($arg['login']) && isset($arg['senha']) && isset($arg['permicao'])) {
            if (strlen($arg['nome']) < 3 && strlen($arg['nome']) > 10) {
                $erro[] = 'Name invalid, this parameter needle min 3 or max 10 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
            if (strlen($arg['nome']) < 3) {
                $erro[] = 'Name invalid, this parameter needle min 3 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'nome','login','senha','permicao': array";
        }

        if (count($erro) == 0) {

            $userSystem = new UsuarioSistema();

            if ($userSystem->where('id_usuario_sistema','=',$create['id_usuario_sistema'])->update($create)) {
                return 'User system update success';
            } else {
                return 'Internal error update user system';
            }

        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }

    public function core_main_delete_user_system()
    {
        $arg = \Request::all();
        $erro = [];
        if (isset($arg['id_usuario_sistema'])) {
           $delete = $arg;
        } else {
            $erro[] = "Has parameters invalid, only acept 'id_usuario_sistema': array";
        }

        if (count($erro) == 0) {

            $userSystem = new UsuarioSistema();

            if ($userSystem->where('id_usuario_sistema','=',$delete['id_usuario_sistema'])->delete()) {
                return 'User system delete success';
            } else {
                return 'Internal error delete user system';
            }

        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }

    public function core_main_get_admin()
    {
        $admins = new Admin();
        $result = $admins->where('ativo','=',true)->get()->toJson();
        return $result;
    }

    public function core_main_create_admin()
    {
        $arg = \Request::all();

        $erro = [];
        $create = [];
        if (isset($arg['nome']) && isset($arg['login']) && isset($arg['senha']) && isset($arg['permicao'])) {
            if (strlen($arg['nome']) < 3 && strlen($arg['nome']) > 10) {
                $erro[] = 'Name invalid, this parameter needle min 3 or max 10 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
            if (strlen($arg['nome']) < 3) {
                $erro[] = 'Name invalid, this parameter needle min 3 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'nome','login','senha','permicao'";
        }
        if (count($erro) == 0) {

            $admin = new Admin();

            if ($admin->create($create)) {
                return 'Admin create success';
            } else {
                return 'Internal error create admin';
            }
        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }
    public function core_main_update_admin()
    {
        $arg = \Request::all();
        $erro = [];
        if (isset($arg['nome']) && isset($arg['login']) && isset($arg['senha']) && isset($arg['permicao'])) {
            if (strlen($arg['nome']) < 3 && strlen($arg['nome']) > 10) {
                $erro[] = 'Name invalid, this parameter needle min 3 or max 10 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
            if (strlen($arg['nome']) < 3) {
                $erro[] = 'Name invalid, this parameter needle min 3 caracters';
            } else {
                $create['nome'] = $arg['nome'];
            }
        } else {
            $erro[] = "Has parameters invalid, only acept 'nome','login','senha','permicao': array";
        }

        if (count($erro) > 0) {

            $admin = new UsuarioSistema();

            if ($admin->where('id_usuario_sistema','=',$create['id_usuario_sistema'])->update($create)) {
                return 'Admin update success';
            } else {
                return 'Internal error update admin';
            }

        } else {
            $e = new ExceptionController($erro);
            return $e->exception();
        }
    }
    public function core_main_get_user()
    {
        $userSystem = new UsuarioSistema();
        $result = $userSystem->where('ativo','=',true)->get()->toJson();
        return $result;
    }

}
