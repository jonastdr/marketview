<?php

namespace App\Http\Middleware;

use Closure;

class Authorization
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header();
        if($header['php-auth-user'][0] == 'marketview' && $header['php-auth-pw'][0] == '123mudar') {
                return $next($request);
        }
        return response('Sem Permissão', 401);
    }
}
