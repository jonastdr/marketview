<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainMenu extends Model
{
	protected $primaryKey = 'id_usuario_sistema';

	protected $table = 'main_menu';

	protected $hidden = ['senha'];

	protected $fillable = [
		'login',
		'senha',
		'nome',
	];
}
