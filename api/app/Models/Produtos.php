<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
	protected $primaryKey = 'id_produto';

	protected $table = 'produto';

	public $timestamps = false;


	protected $fillable = [
		'valor',
		'descricao'
	];
}
