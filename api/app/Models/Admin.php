<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	protected $primaryKey = 'id_admin';

	protected $table = 'admin';

	protected $hidden = ['senha'];

	protected $fillable = [
		'login',
		'senha',
		'nome',
	];
}
