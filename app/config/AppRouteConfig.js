app.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.when('', 'home');

    $stateProvider
        .state('home', {
            url: '/home',
            views: {
                main: {
                    controller: 'HomeCtrl',
                    templateUrl: 'modules/index.html'
                }
            }
        })

        .state('lojas', {
            url: '/lojas',
            views: {
                main: {
                    controller: 'LojasCtrl',
                    templateUrl: 'modules/lojas/index.html'
                }
            }
        })

        //Produtos
        .state('produtos', {
            url: '/produtos',
            views: {
                main: {
                    controller: 'ProdutosCtrl',
                    templateUrl: 'modules/produtos/index.html'
                }
            }
        })

        .state('produtos/show', {
            url: '/show/:id_produto',
            views: {
                main: {
                    controller: 'ProdutosCtrl',
                    templateUrl: 'modules/produtos/show.html'
                }
            }
        })

        .state('produtos/add', {
            url: '/add/:add',
            main: {
                controller: 'ProdutosCtrl',
                templateUrl: 'modules/produtos/show.html'
            }
        })

        .state('configuracoes', {
            url: '/configuracoes',
            views: {
                main: {
                    controller: 'ConfiguracoesCtrl',
                    templateUrl: 'modules/configuracoes/index.html'
                }
            }
        })
    ;
});