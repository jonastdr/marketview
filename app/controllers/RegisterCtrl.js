app.controller('RegisterCtrl', function ($scope, $rootScope, $http, $stateParams, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };

    $scope.register = function() {
        alert('Registrado');

        $scope.hide();
    };
});

app.controller('RegisterCtrl.login', function ($scope, $rootScope, $http, $stateParams, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };

    $scope.register = function() {
        alert('Logado!');

        $scope.hide();
    };
});