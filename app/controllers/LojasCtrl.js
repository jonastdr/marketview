app.controller('LojasCtrl', function ($scope, $rootScope, $http) {
    $rootScope.title = 'Lojas';

    var lojas = {
        add: function(obj) {
            $http.put($rootScope.api(['create', 'store.php']), obj).success(function (response) {

            }).error(function (e) {
                console.log(e);
            });
        },
        save: function (obj) {
            $http.post($rootScope.api(['update', 'store.php']), obj).success(function (response) {

            }).error(function (e) {
                console.log(e);
            });
        },
        del: function (obj) {
            $http.delete($rootScope.api(['delete', 'store.php'], obj)).success(function (response) {

            }).error(function (e) {
                console.log(e);
            });
        },
        get: function (obj) {
            $http.get($rootScope.api(['get', 'store.php', obj.id_produto])).success(function (response) {

            }).error(function (e) {
                console.log(e);
            });
        },
        list: function () {
            $http.get($rootScope.api(['list', 'store.php'])).success(function (response) {
                $scope.lojas = response;
            }).error(function (e) {
                console.log(e);
            });
        },
        search: function (form) {
            $http.get($rootScope.api(['search', 'store.php']), form).success(function (response) {

            }).error(function (e) {
                console.log(e);
            });
        }
    };

    //Lista todas as lojas
    lojas.list();
});