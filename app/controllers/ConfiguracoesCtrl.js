app.controller('ConfiguracoesCtrl', function ($scope, $rootScope, $http) {
    $rootScope.title = 'Configurações';

    $rootScope.subMenu = [
        {
            titulo: 'Logar',
            link: 'link'
        },
        {
            titulo: 'Usuário',
            link: 'link'
        }
    ];

    var usuarios = {
        add: function(obj) {
            $http.put($rootScope.api(['create', 'user']), obj);
        },
        save: function (obj) {
            $http.post($rootScope.api(['update', 'user']), obj);
        },
        del: function (obj) {
            $http.delete($rootScope.api(['delete', 'user'], obj));
        },
        get: function (obj) {
            $http.get($rootScope.api(['get', 'user', obj.id_produto]));
        },
        list: function () {
            $http.get($rootScope.api(['list', 'user']));
        }
    };

    $scope.configs = [
        {
            text: 'Config',
            checked: true
        },
        {
            text: 'Config',
            checked: false
        },
        {
            text: 'Config',
            checked: false
        },
        {
            text: 'Config',
            checked: false
        },
        {
            text: 'Config',
            checked: false
        },
        {
            text: 'Config',
            checked: false
        },

    ];
});