app.controller('HomeCtrl', function ($scope, $rootScope, $http) {
    $rootScope.title = 'MarketView';

    $rootScope.subMenu = [
        {
            titulo: 'Logar',
            link: 'openLogin($event)'
        },
        {
            titulo: 'Registre-se',
            link: 'openRegister($event)'
        },
    ];

    $scope.produtos = $scope.$parent.produtos;
});