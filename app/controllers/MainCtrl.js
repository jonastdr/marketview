app.controller('MainCtrl', function($scope, $rootScope, $http, $mdSidenav, $state, $mdDialog) {
    $scope.menus = [
        {
            name: 'Home',
            link: 'home'
        },
        {
            name: 'Lojas',
            link: 'lojas'
        },
        {
            name: 'Produtos',
            link: 'produtos'
        },
        {
            name: 'Configurações',
            link: 'configuracoes'
        }
    ];

    $scope.user = 'Jonas';

    //Funções de menu
    $scope.toggleMenu = function () {
        $mdSidenav('menu').toggle();
    };

    $scope.closeMenu = function() {
        $mdSidenav('menu').close();
    }

    $scope.isOpenRight = function(){
        return $mdSidenav('menu').isOpen();
    };
    //Fim funções de menu

    $scope.link = function (path, params) {
        $state.go(path, params);
    }


    $rootScope.openRegister = function (ev) {
        $mdDialog.show({
            controller: 'RegisterCtrl',
            templateUrl: 'modules/cadastro/register.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
        });
    };

    $rootScope.openLogin = function (ev) {
        $mdDialog.show({
            controller: 'RegisterCtrl.login',
            templateUrl: 'modules/cadastro/login.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
        });
    };

    //API
    var API = {
        global:{
            api: function (method,paths,data) {
                //http://mv-api.projectws.kinghost.net/
                return {
                    method: typeof method === undefined ? 'GET' : method,
                    data: typeof data === undefined ? '' : data,
                    url:'http://marketview.dev/api/public/' + paths.join('/'),
                    headers: {
                        'Authorization': 'Basic bWFya2V0dmlldzoxMjNtdWRhcg=='
                    }
                };
            },
            http: function (method, paths, data, success, error) {
                $http(this.api(method,paths, data)).success(function (response) {
                    success(response);
                }).error(function (e) {
                    error(e);
                });
            }
        },
        products: {
            get: function (id_produto, success, error) {
                API.global.http('GET', ['get', 'products',id_produto], undefined, success, error);
            },
            list: function (success, error) {
                API.global.http('GET', ['list', 'products'], undefined, success, error);
            },
        }
    };

    $rootScope.api = API;
});