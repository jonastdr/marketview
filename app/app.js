var app = angular.module('MarketView', [
    'ui.router',
    'ngMaterial'
]).config(function($httpProvider) {
    $httpProvider.defaults.headers.common['Authorization'] = 'Basic admin:123mudar';
});